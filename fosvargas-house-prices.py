from fastapi import FastAPI
from pydantic import BaseModel
import pickle
import pandas as pd
import numpy as np
import uvicorn

app = FastAPI()

model = pickle.load(open('rf_model.pkl', 'rb'))

class HPInput(BaseModel):
    BsmtFinSF1: float
    GarageYrBlt: float
    FirstFlrSF: float
    GarageArea: float
    TotalBsmtSF: float
    YearBuilt: int
    GarageCars: int
    GrLivArea: float
    OverallQual: float

class HPOutput(BaseModel):
    predicted_price: float

@app.get('/')
def index():
    return {'mensaje': 'APP para predecir los precios de casas'}


@app.post('/prediccion', response_model = HPOutput, status_code = 201)
def prediction(input: HPInput):

    input_values = [input.BsmtFinSF1,
                    input.GarageYrBlt,
                    input.FirstFlrSF,
                    input.GarageArea,
                    input.TotalBsmtSF,
                    input.YearBuilt,
                    input.GarageCars,
                    input.GrLivArea,
                    input.OverallQual];
    
    input = [np.array(input_values)]
    input_df = pd.DataFrame(input)
    
    prediction= np.round(model.predict(input_df),2)
    
    return HPOutput(predicted_price = prediction)

if __name__ == '__main__':
    uvicorn.run(app, host="0.0.0.0", port=8000, debug=True)
